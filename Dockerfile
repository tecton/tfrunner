FROM bitnami/minideb:jessie
ARG TERRAFORM_VERSION=0.13.3
RUN apt-get update \
    && apt-get install -y curl git unzip \
    && rm -rf /var/lib/apt/lists/*
RUN curl -o aws-iam-authenticator https://amazon-eks.s3.us-west-2.amazonaws.com/1.18.8/2020-09-18/bin/linux/amd64/aws-iam-authenticator \
    && chmod +x aws-iam-authenticator \
    && mv aws-iam-authenticator /opt/aws-iam-authenticator
RUN curl "https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip" -o terraform.zip \
    && unzip terraform.zip -d /opt \
    && rm terraform.zip
ENV PATH=/opt:$PATH
