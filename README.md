# TFRunner

A simple Docker image that has some baked in tools for running Terraform in CI/CD.

My use-case for this was applying some Terraform which in turn downloaded kubectl & applied some manifests to an EKS cluster.  Most notably this required aws-iam-authenticator to be available.